import { Component, OnInit, Input} from '@angular/core';
import { Node } from 'src/app/core/node';
import { NodeService } from 'src/app/service/node.service';

@Component({
  selector: 'app-node',
  templateUrl: './node.component.html',
  styleUrls: ['./node.component.scss']
})
export class NodeComponent implements OnInit {

  @Input() data: Node;

  childText = "";
  showAddForm = false;

  constructor(private nodeService: NodeService) { }

  ngOnInit(): void {
  }

  addChild(): void{
    this.data.children.unshift(new Node(this.nodeService.nextId, this.data.id, this.childText, this.data.depth + 1));
    this.showAddForm = false;
  }

}
