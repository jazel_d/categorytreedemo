import { Node } from "../core/node";
import { NodeService } from "../service/node.service";
import {TestBed, ComponentFixture} from '@angular/core/testing';

describe('Node test', () => {

    let ns: NodeService;

    let parent: Node;
    let child1: Node;
    let child2: Node;
    let child3: Node;

    beforeEach(() => {
        
        TestBed.configureTestingModule({
            providers: [NodeService]
        });

        ns = TestBed.get(NodeService);

        parent = new Node(5, null, "root", 1);
        child1 = new Node(67, 5, "child 1", 2);
        child2 = new Node(9, 5, "child 2", 2);
        child3 = new Node(25, 5, "child 3", 2);
    });

    it('child array always initialized', () => {      
        expect(Array.isArray(parent.children)).toEqual(true);
        expect(parent.children.length).toBe(0);
    });

    it('checks for nulls', () => {
        ns.addChild(parent, null);
        ns.addChild(null, parent);        
        ns.addChild(null, null);
        expect(parent.children.length).toEqual(0);
    });

    it('can push to parent', () => {
        ns.addChild(parent, child1);
        ns.addChild(parent, child2);
        ns.addChild(parent, child3);
        expect(parent.children.length).toBe(3);
    });
})