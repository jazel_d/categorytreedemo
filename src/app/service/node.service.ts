import { Injectable, NgModuleDecorator } from '@angular/core';
import { Node } from '../core/node';

@Injectable({
  providedIn: 'root'
})
export class NodeService {

  private _nodes: any[] = [];
  private _nextId: number = 0;


  get Nodes(): Node[] {
    return this._nodes;
  }
  set Nodes(input: Node[]) {
    this._nodes = input;
  }

  get nextId(): number {
    return Math.floor(Math.random() * 100000000);
  }

  constructor() { 
  }

  loadData():void{
    this._nodes = [
      
      { id: 1, text: "Lorem",        parentId: null,  depth:1, children: [        
        { id: 2, text: "Ipsum",        parentId: 1,     depth: 2, children: [] },
        { id: 3, text: "Dolor",        parentId: 1,     depth: 2, children: [
          { id: 6, text: "Orci",        parentId: 3,     depth: 3, children: [
            { id: 7, text: "Quis",        parentId: 6,     depth: 4, children: [
              { id: 8, text: "Odio",        parentId: 7,     depth: 5, children: [] },
            ] },            
          ] },
        ] },
        { id: 4, text: "Sit",          parentId: 1,     depth: 2, children: [
          { id: 9, text: "Amet",        parentId: 4,     depth: 3, children: [] },
          { id: 10, text: "Consectetur",parentId: 4,     depth: 3, children: [] },
        ] },
        { id: 5, text: "Adipiscing",   parentId: 1,     depth: 2, children: [
          { id: 11, text: "Elit",        parentId: 5,     depth: 3, children: [
            { id: 12, text: "Vestibulum",         parentId: 11,     depth: 4, children: [] },
            { id: 13, text: "Vitae",              parentId: 11,     depth: 4, children: [] },
          ] },

        ] },
      ] }
    ];
  }

  addChild(parent:Node, child: Node): boolean{
    if(!child || !parent)
        return false;

    // call validation here
    parent.children.unshift(child);
    return true;
  }

}
