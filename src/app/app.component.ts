import { Component, OnInit } from '@angular/core';
import { NodeService } from './service/node.service';
import { Node } from './core/node';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  
  constructor(private nodeService: NodeService) { }

  allNodes: Node[] = [];
  content: string;
  showDebug = false;

  nextIdCounter = 1;

  ngOnInit(): void {
    this.nodeService.loadData();
    this.allNodes = this.nodeService.Nodes;
  }
  
}


