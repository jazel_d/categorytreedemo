export class Node{
    
    id: number;
    text: string;
    parentId: number;
    children: Node[];
    depth: number;

    constructor(id: number, parentId: number, text: string, depth: number) {
        this.id = id;
        this.parentId = parentId;
        this.text = text;
        this.children = [];
        this.depth = depth;
    }
}