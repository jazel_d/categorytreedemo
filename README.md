# Pre requirements
Install Angular CLI: [https://angular.io/cli](https://angular.io/cli)
```bash
npm install -g @angular/cli
```
Typescript: [https://www.npmjs.com/package/typescript](https://www.npmjs.com/package/typescript)
```bash
npm install -g typescript
```
SASS: [https://sass-lang.com/install](https://sass-lang.com/install)
```bash
npm install -g sass
```
## Run application `ng serve`
Application runs on deffault port: `4200`. Use `ng serve --port 4401` if you want to run it on another port instead.

All run scripts are available in `package.json` file

Default start command:
```cli
npm run start
```
Run all tests:

```cli
npm run test
```
